class PagesController < ApplicationController
  def home
    render "home", :layout => 'application'
  end

  def terms
  end

  def about
  end

  def faq
  end

  def privacy
  end

  def contact
  end
end
