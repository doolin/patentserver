# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :invpat do
    firstname "MyString"
    middlename "MyString"
    lastname "MyString"
    patent "MyString"
    invseq 1
  end
end
